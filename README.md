# Refactor done in this assignment
1. File Name: post_product.feature
    - update feature file get call with products Cola and Pasta (product Apple is returning emty [] responcse)
2. File Name: SearchStepDefinitions.java
    - line 19 and 24 -- Assertion is updated to validate the given value present in collection
    - line 29 -- Assertion is updated to assert Boolean value
3. File Name: .gitlab-ci.yml
    - Added this file for pipeline 
4. Removed other unwanted files


# Prerequisite to Run the Test in Local
    - Java (JDK 11)
    - Maven

# To run the test
run 'mvn clean install -DTest=TestRunner'

# To Write new Test

1. Feature files are available in 'src/test/resources/features' path, create new folder for each controller and create feature file for your scenarios
2. Step/glue is available in 'src/test/java/starter/stepdefinitions', create step definition for the gerkin steps
3. Runner file is available 'src/test/java/starter/TestRunner.java', update the runner according to your need of test run






