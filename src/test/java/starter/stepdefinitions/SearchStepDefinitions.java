package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;

import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String arg0) {
        SerenityRest.given().get(arg0);
    }

    @Then("he sees the results displayed for pasta")
    public void heSeesTheResultsDisplayedForPasta() {
        restAssuredThat(response -> response.statusCode(200).and().body("title", hasItem("PLUS Mini penne volkoren")));
    }

    @Then("he sees the results displayed for cola")
    public void heSeesTheResultsDisplayedForCola() {
        restAssuredThat(response -> response.statusCode(200).and().body("title", hasItem("First Choice Cola regular")));
    }

    @Then("he doesn not see the results")
    public void he_Doesn_Not_See_The_Results() {
        restAssuredThat(response -> response.body("detail.error", is(true)));

    }
}
